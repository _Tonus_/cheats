~A              tous
~b EXPR         contenant EXPR dans leur corps
~B EXPR         contenant EXPR (intégralité du )
~c USER         avec une copie carbone (cc:) pour USER
~C EXPR         est destiné à (to:) ou en copie (cc:) pour EXPR
~D              effacés
~d [MIN]-[MAX]  avec ``date-d-envoi'' dans l'intervalle Date
~E              arrivés à expiration
~e EXPR         contenant EXPR dans le champ `'Sender''
~F              marqués important
~f USER         provenant de USER
~g              signés (PGP)
~G              chiffrés (PGP)
~h EXPR         contenant EXPR dans leurs en-têtes
~k              contenant une clé PGP
~i ID           avec ID dans le champ ``Message-ID''
~L EXPR         provenant de ou reçu par EXPR
~l              adressé à une liste de diffusion connue
~m [MIN]-[MAX]  dans l'intervalle MIN à MAX *)
~n [MIN]-[MAX]  avec un score dans l'intervalle MIN à MAX *)
~N              nouveaux 
~O              anciens 
~p              qui vous est adressé (voir $alternates)
~P              que vous avez envoyé (voir $alternates)
~Q              auxquels vous avez répondu
~R              lus
~r [MIN]-[MAX]  avec `'date-reception'' dans l'intervalle Date
~S              remplacés (supersede)
~s SUBJECT      ayant SUBJECT dans l'objet (champ ``Subject'').
~T              marqués (tag)
~t USER         adressés à USER
~U              non lus
~v              faisant partie d'une enfilade repliée
~x EXPR         contenant EXPR dans le champ `References'
~y EXPR         contenant EXPR dans le champ `X-Label'
~z [MIN]-[MAX]  avec une taille dans l'intervalle MIN à MAX *)
~=              doublons (voir $duplicate_threads)
