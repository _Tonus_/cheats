## to have share mounted change /etc/rc.local
insmod /lib/modules/$(uname -r)/kernel/fs/9p/9p.ko && mount -t 9p -o trans=virtio,version=9p2000.L repos /mnt/hd

## shorten lilo, edit /etc/lilo.conf
timeout=20
## and run
lilo



# various tests

#mount -o loop,offset=368050176 /mnt/vms/win/win7.img /mnt/hd
#qemu-system-x86_64 -m 3072 -k fr -drive file=/mnt/vms/win/win7.img,format=raw -cdrom /mnt/Media-old/Android/Windows\ 7\ AIO\ 32\&64bits\ R2.iso -boot menu=on,order=cd -fsdev local,id=dl0,path=/home/antoine/Downloads/,security_model=passthrough -device virtio-9p-pci,fsdev=dl0,mount_tag=dl0 -fsdev local,id=repo0,path=/mnt/repo/,security_model=passthrough -device virtio-9p-pci,fsdev=repo0,mount_tag=repo0 -fsdev local,id=media0,path=/mnt/Media-old/,security_model=passthrough -device virtio-9p-pci,fsdev=media0,mount_tag=media0
#qemu-system-x86_64 -enable-kvm -m 4096 -vga std -k fr -daemonize -drive file=/mnt/vms/win/win7.img,format=raw -cdrom /mnt/Media-old/Android/Windows\ 7\ AIO\ 32\&64bits\ R2.iso -boot menu=on,order=cd -usb -device usb-host,hostbus=1,hostaddr=8
#qemu-system-x86_64 -enable-kvm -m 4096 -vga std -k fr -daemonize -drive file=/mnt/vms/win/win7.img,format=raw -cdrom /mnt/Media-old/Android/Windows\ 7\ AIO\ 32\&64bits\ R2.iso -boot menu=on,order=cd -usb -device usb-host,hostbus=1,hostaddr=10 -usbdevice host:04e8:685d
#qemu-system-x86_64 -enable-kvm -m 4096 -vga std -k fr -daemonize -drive file=/mnt/vms/win/win7.img,format=raw -boot menu=on -usb -device usb-ehci,id=ehci -device nec-usb-xhci,id=xhci'-cdrom /home/antoine/Downloads/OFF\ 2013\ X86\ SP1\ Select-JBDL.iso -fsdev local,id=dl0,path=/home/antoine/Downloads/,security_model=passthrough -device virtio-9p-pci,fsdev=dl0,mount_tag=dl0 -fsdev local,id=repo0,path=/mnt/repo/,security_model=passthrough -device virtio-9p-pci,fsdev=repo0,mount_tag=repo0 -fsdev local,id=media0,path=/mnt/Media-old/,security_model=passthrough -device virtio-9p-pci,fsdev=media0,mount_tag=media0
#qemu-system-x86_64 -enable-kvm -m 4096 -vga std -k fr -daemonize -drive file=/mnt/vms/win/win7.img,format=raw -boot menu=on -usb -device usb-ehci,id=ehci -device nec-usb-xhci,id=xhci -cdrom /home/antoine/Downloads/fr_office_professional_plus_2013_x64_dvd_1134000.iso -fsdev local,id=dl0,path=/home/antoine/Downloads/,security_model=passthrough -device virtio-9p-pci,fsdev=dl0,mount_tag=dl0 -fsdev local,id=repo0,path=/mnt/repo/,security_model=passthrough -device virtio-9p-pci,fsdev=repo0,mount_tag=repo0 -fsdev local,id=media0,path=/mnt/Media-old/,security_model=passthrough -device virtio-9p-pci,fsdev=media0,mount_tag=media0
#qemu-system-x86_64 -enable-kvm -m 4096 -vga std -k fr -daemonize -drive file=/mnt/vms/win/win7.img,format=raw -boot menu=on -usb -device usb-ehci,id=ehci -device nec-usb-xhci,id=xhci -cdrom /home/antoine/Downloads/fr_office_professional_plus_2013_x86_dvd_1134001.iso -fsdev local,id=dl0,path=/home/antoine/Downloads/,security_model=passthrough -device virtio-9p-pci,fsdev=dl0,mount_tag=dl0 -fsdev local,id=repo0,path=/mnt/repo/,security_model=passthrough -device virtio-9p-pci,fsdev=repo0,mount_tag=repo0 -fsdev local,id=media0,path=/mnt/Media-old/,security_model=passthrough -device virtio-9p-pci,fsdev=media0,mount_tag=media0

*************************************************
*********** WORKING COMMAND *********************
***********    WINDOWS 7    *********************
*************************************************

qemu-system-x86_64 -enable-kvm -m 4096 -vga std -k fr -daemonize -drive file=/mnt/vms/win/win7.img,format=raw -boot menu=on -usb -device usb-ehci,id=ehci -device nec-usb-xhci,id=xhci -cdrom /home/antoine/Downloads/fr_office_professional_plus_2013_x64_dvd_1134000.iso -fsdev local,id=dl0,path=/home/antoine/Downloads/,security_model=passthrough

*************************************************
*********** WORKING COMMAND *********************
***********      PLASMA     *********************
*************************************************

qemu-system-x86_64 -enable-kvm -m 4096 -vga std -k fr -daemonize -boot menu=on -usb -device usb-ehci,id=ehci -device nec-usb-xhci,id=xhci -cdrom /mnt/Media/disk_img/slackware64-live-plasma5-current.iso -fsdev local,id=dl0,path=/home/antoine/Downloads/,security_model=passthrough

#
# BASE SCRIPT
#
i3-msg workspace "7  ; append_layout  ~/.config/i3/scripts/qemu_layout.json"
xfce4-terminal -T qemu -e "qemu-system-x86_64 -enable-kvm -smp cpus=2 -smp maxcpus=3 -m 4096 -k fr -daemonize -vga virtio -boot menu=on -usb -drive file=/mnt/Media/disk_img/slackware/slackware64-current_base.img,format=raw -cdrom /mnt/Media/disk_img/slackware64-current-iso/slackware64-current-install-dvd.iso -fsdev local,id=repos,path=/mnt/repo/,security_model=none -device virtio-9p-pci,fsdev=repos,mount_tag=repos"

## with 2 shares
#qemu-system-x86_64 -enable-kvm -smp cpus=2 -smp maxcpus=3 -m 4096 -k fr -daemonize -vga virtio -boot menu=on -usb -drive file=/mnt/vms/slackware/slackware64-current.img,format=raw -cdrom /mnt/Media/disk_img/slackware64-current-install-dvd.iso -fsdev local,id=repo,path=/mnt/repo/Sandbox,security_model=none -device virtio-9p-pci,fsdev=repo,mount_tag=repo -fsdev local,id=SBo,path=/tmp/SBo-clean,security_model=none -device virtio-9p-pci,fsdev=SBo,mount_tag=SBo && i3-msg workspace "7 "

#
# CLEAN SNAPSHOT SCRIPT
#

#!/bin/sh
#
# Start Slackware in QEMU
# Use QEMU snapshots (COW - Copy On Write images) for the purpose of
# testing software in a clean environment.

## ---------------------------------------------------------------------------
## Create an empty 15GB image file like this:
## # dd if=/dev/zero of=slackware.img bs=1k count=15000000
##
## You must install a version of Slackware in this image file.
## This installation will be your base image. It will not change unless
## you want to install security patches.
##
## Then create the QCOW (qemu copy-on-write) file like this:
## $ qemu-img create -b slackware.img -f qcow slackware_snapshot.qcow
##
## DO NOT commit the changes made in the QCOW file back to the base image!
## The QCOW image is only used once and re-created every time the script runs!
## ---------------------------------------------------------------------------

# Location of your QEMU images:
IMAGEDIR=/mnt/Media/disk_img/slackware

#[ ! -z $* ]  && PARAMS=$*
PARAMS=$*

# Qemu can use SDL sound instead of the default OSS
export QEMU_AUDIO_DRV=sdl

# Whereas SDL can play through alsa:
export SDL_AUDIODRIVER=alsa

cd $IMAGEDIR
# Remove old QCOW file, create a new one:
rm -f slackware_snapshot.qcow
qemu-img create -b slackware64-current_base.img -f qcow slackware64-current_snapshot.qcow
# Start QEMU with the fresh image:
#qemu -m 256 -localtime -usb -soundhw all -kernel-kqemu \
#        -hda slackware_snapshot.qcow ${PARAMS} \
#        >slackware_snapshot.log 2>slackware_snapshot.err &

#qemu-system-x86_64 -enable-kvm -smp cpus=2 -smp maxcpus=3 -m 4096 -k fr -daemonize -vga std -boot menu=on -usb -drive file=/mnt/vms/slackware/slackware64-current_clean.img,format=raw -cdrom /mnt/Media/disk_img/slackware64-current-install-dvd.iso -fsdev local,id=repo,path=/mnt/repo/Sandbox,security_model=none -device virtio-9p-pci,fsdev=repo,mount_tag=repo && i3-msg workspace "7 "
i3-msg workspace "7 ; append_layout  ~/.config/i3/scripts/qemu_layout.json"
xfce4-terminal -T qemu -e "qemu-system-x86_64 -enable-kvm -smp cpus=2 -smp maxcpus=3 \
				-m 4096 -k fr -daemonize -vga virtio -boot menu=on \
				-usb -drive file=slackware64-current_snapshot.qcow ${PARAMS} \
				-cdrom slackware64-current-iso/slackware64-current-install-dvd.iso \
				-fsdev local,id=repo,path=/mnt/repo/Sandbox,security_model=none \
				-device virtio-9p-pci,fsdev=repo,mount_tag=repo \
				>slackware_snapshot.log 2>slackware_snapshot.err &"
